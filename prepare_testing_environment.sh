#!/usr/bin/env bash

credentials_filename="credentials.text"

_create_credentials() {
    name="${1}"
    namespace="${2}"
    public_api_key="${3}"
    private_api_key="${4}"

    kubectl --namespace "${namespace}" delete "secret/${name}" &> /dev/null || true

    kubectl --namespace "${namespace}" create secret generic "${name}" \
            --from-literal=user="${public_api_key=}" --from-literal=publicApiKey="${private_api_key}"
}

_create_project() {
    name="${1}"
    namespace="${2}"

    base_url="${3}"
    credentials="${4}"
    org_id="${5}"

    project_opts=(
        "--from-literal=projectName=${name}"
        "--from-literal=baseUrl=${base_url}"
        "--from-literal=credentials=${credentials}"
        "--from-literal=orgId=${org_id}"
    )

    kubectl --namespace "${namespace}" delete "configmap/project" &> /dev/null || true

    kubectl --namespace "${namespace}" create configmap project "${project_opts[@]}"

}

_create_namespace() {
    namespace="${1}"

    kubectl create namespace "${namespace}" &> /dev/null || true
}

_export_kubectl_config() {
    cluster_name="${1}"

    kops export kubecfg "${cluster_name}"
}

# Create namespaces for the tests
create_testing_namespaces() {
    _create_namespace "project-0"
    _create_namespace "project-1"
    _create_namespace "project-2"
    _create_namespace "project-3"
}

_read_base_url() {
    base_url=$(head "${credentials_filename=}" -n 1)

    echo "${base_url}"
}

_create_project_credentials() {
    project_name="${1}"
    namespace="${2}"

    public_api_key=$(grep "${project_name=}" "${credentials_filename=}" -A 3 | awk 'NR==2')
    private_api_key=$(grep "${project_name=}" "${credentials_filename=}" -A 3 | awk 'NR==3')

    _create_credentials "credentials" "${namespace}" "${public_api_key}" "${private_api_key}"
}

_get_org_id() {
    echo "5d0bbee6b20d641109a11224"
}

_install_operator() {
    project="${1}"
    kubectl apply -f https://raw.githubusercontent.com/mongodb/mongodb-enterprise-kubernetes/master/crds.yaml &> /dev/null || true

    kubectl --namespace "${project}" apply -f https://gist.githubusercontent.com/rodrigovalin/842b96815afca6389c977f63871984f3/raw/86c2f30bbc3a388821ac713b914f8018bb589c15/operator.yaml &> /dev/null
}

initialize() {
    create_testing_namespaces
    base_url=$(_read_base_url)
    org_id=$(_get_org_id)

    # Creating projects & credentials everywhere
    for i in $(seq 0 3); do
        _create_project_credentials "Project ${i}" "project-${i}"
        _create_project "Project ${i}" "project-${i}" "${base_url}" "credentials" "${org_id}"
        _install_operator "project-${i}"
    done
}

reset() {
    for i in $(seq 0 3); do
        kubectl delete "ns/project-${i}"
    done
}

initialize
