#!/usr/bin/env bash

create_cluster() {
    source ~/workspace/10gen/mdb-operator-scripts/create_license_management_testing_env.sh

    _create_cluster
}

_wait_for_cluster() {
    name="${1}"
    while ! kops validate cluster "${name}"; do printf "."; sleep 3; done
}

_create_cluster() {
    node_count=4
    # zones="us-east-2a,us-east-2b,us-east-2c"
    zones="eu-west-1a"
    name="lm-testing-cluster.mongokubernetes.com"
    host_size="t2.2xlarge"
    volume_size=32

    kops create cluster \
         "${name}" \
         --node-count "${node_count}" \
         --zones "${zones}" \
         --node-size "${host_size}" \
         --master-size="${host_size}" \
         --node-volume-size "${volume_size}" \
         --master-volume-size="${volume_size}"  \
         --kubernetes-version=v1.11.6 \
         --ssh-public-key=~/.ssh/id_rsa.pub \
         --authorization RBAC

    kops create secret --name "${name}" sshpublickey admin -i ~/.ssh/id_rsa.pub
    kops update cluster "${name}" --yes

    _wait_for_cluster ${name}
    # kops validate cluster "${name}"

    kubectl create serviceaccount dashboard -n default
    kubectl create clusterrolebinding dashboard-admin -n default \
            --clusterrole=cluster-admin \
            --serviceaccount=default:dashboard
    kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
}
